﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PatchworkGenerator.Patchwork;

namespace PatchworkGenerator
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int _Columns = 16;
        private int _Rows = 16;

        public MainWindow()
        {
            PatwchorkColors = new[]
{
                Colors.Violet,
                Colors.Blue,
                Colors.Gray,
                Colors.Red,
            };

            InitializeComponent();
            DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public int Columns
        {
            get
            {
                return _Columns;
            }
            set
            {
                _Columns = value;
                OnPropertyChanged(nameof(Columns));
            }
        }

        public int Rows
        {
            get
            {
                return _Rows;
            }
            set
            {
                _Rows = value;
                OnPropertyChanged(nameof(Rows));
            }
        }

        public Color[] PatwchorkColors { get; set; } = new Color[4];

        private void Exit_Click(object sender, RoutedEventArgs e)
        {


            PatchworkGrid pat = new PatchworkGrid(Columns, Rows);
            pat.AvailableColors = PatwchorkColors;
            pat.SetCornerColors();
            pat.PropagateColor();

            List<List<Color>> lsts = new List<List<Color>>();

            for (int i = 0; i < pat.Row; i++)
            {
                lsts.Add(new List<Color>());

                for (int j = 0; j < pat.Column; j++)
                {
                    lsts[i].Add(pat.ColorArray[i,j]);
                }
            }

            lst.ItemsSource = lsts;
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
