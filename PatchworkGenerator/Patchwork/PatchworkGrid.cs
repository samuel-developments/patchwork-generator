﻿using PatchworkGenerator.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PatchworkGenerator.Patchwork
{
    class PatchworkGrid
    {
        public int Column { get; set; }

        public int Row { get; set; }

        public Color[,] ColorArray { get; set; }

        public Color[] AvailableColors { get; set; } = new Color[4];

        public PatchworkGrid(int col, int row)
        {
            Column = col;
            Row = row;

            ColorArray = new Color[Row, Column];
        }



        public void SetCornerColors()
        {
            List<int> indices = new List<int> { 0, 1, 2, 3 };

            indices.Shuffle();

            ColorArray[0, 0] = AvailableColors[indices[0]];
            ColorArray[0, Column - 1] = AvailableColors[indices[1]];
            ColorArray[Row - 1,0] = AvailableColors[indices[2]];
            ColorArray[Row - 1, Column - 1] = AvailableColors[indices[3]];

        }

        public void PropagateColor()
        {
            for (int i = 0; i < Row; i++)
            {
                
                for (int j = 0; j < Column; j++)
                {
                    // Get Neighbors
                    //SetNeighbors(i, j);
                    if (ColorArray[i, j] == Color.FromArgb(0, 0, 0, 0))
                    {
                        var possibleColors = FindColorAround(i, j);
                        possibleColors.Shuffle();


                            ColorArray[i, j] = possibleColors.First();
                        
                    }


                }
            }
        }




        private List<Color> FindColorAround(int coordX, int coordY)
        {
            // Get Available colors
            Color[] usable = new Color[4];
            AvailableColors.CopyTo(usable, 0);

            List<Color> result = usable.ToList();
            //dx and dy contains a vector of all possible moves: E, NE, N, NW, W, SW, S, SE.
            /*
            int[] dx = { 1, 1, 0, -1, -1, -1, 0, 1 };
            int[] dy = { 0, -1, -1, -1, 0, 1, 1, 1 };
            */

            int[] dx = { 1,  0, -1, 0 };
            int[] dy = { 0, -1,  0, 1};
            for (int k = 0; k < dx.Length; k++)
            {
                int i = dx[k] + coordX;
                int j = dy[k] + coordY;
                if (IsInBoundary(i, j, ColorArray))
                {
                    if(ColorArray[i, j] != Color.FromArgb(0, 0, 0, 0)) // if neighbor is not colorless
                    {
                        result.Remove(ColorArray[i, j]);
                    }
                }
            }

            return result;
        }

        //To check that you are within the bounds of the matrix
        public bool IsInBoundary(int i, int j, Color[,] matrix)
        {
            return i >= 0 && i < Row && j >= 0 && j < Column;
        }


    }
}
