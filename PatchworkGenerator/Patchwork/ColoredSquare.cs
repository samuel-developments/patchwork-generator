﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatchworkGenerator.Patchwork
{
    class ColoredSquare
    {
        public List<ColoredSquare> CloseSquares { get; set; } = new List<ColoredSquare>();
    }
}
