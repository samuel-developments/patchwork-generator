# Patchwork editor
This project is a simple project for fun and for helping my fianc�. The idea is to generate an NxM matrix with C colors. Each square of the matrix is randomly one of the colors. The following should be respected:

- Each corner should be of a different color (if > 4 color)
- No color should be adjacent to each other
- No color should be in diagonal to another
- It should be possible to edit the matrix

## Current Status

The current status is under development. The basics are present but a lot of work is left. Please, see the issue tracking to see more.
